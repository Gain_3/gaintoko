<nav class="navbar fixed-top navbar-light bg-light">
  <a href="#" class="navbar-brand">Gain Toko</a>
  <div class="navbar-expand nav1">
    <ul class="nav navbar-nav float-right">
      <li class="nav-item nav2">
        <a href="../toko-register.php" class="nav-link">Daftar</a>
      </li>
      <li class="nav-item nav2">
        <a href="#" class="nav-link">Masuk</a>
      </li>
    </ul>
    <form class="navbar-search float-right form-search">
      <input type="search" name="search" placeholder="Cari" class="search">
      <button class="btn btn-success cari text-center " type="submit">Cari</button>
    </form>
  </div>
</nav>